# usedrip

[![Codacy Badge](https://app.codacy.com/project/badge/Grade/4f2ac38277a14f67994ad16582468bf4)](https://www.codacy.com/gl/diegosaouda/usedrip-test-bank/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=diegosaouda/usedrip-test-bank&amp;utm_campaign=Badge_Grade)

## Table Of Contents

- [Purpose](#purpose)
- [Install Docker](#install-docker)
- [Run](#run)
- [Using and Testing](#using-and-testing)
- [Run Integration and Unit Tests](#run-integration-and-unit-tests)

## Purpose

The purpose of this code test is to show us your skills 

- Problem solving 
- Code structure and OOP concepts 
- Reliability and Testing

Please keep these aspects in mind as you develop your solution. Also, your chosen implementation doesn't necessarily have to be the best you can think of, but one that you can complete in the allocated time.

**Instructions**

- The time for completing this test is 3days. Let us know if you need more time
- You can use the language you feel most comfortable with. Ps. Our current language is Java + Spring 
- You must send your code + the instruction to run it
- Code should be tested

**Important information**

For us it will be equally important the quality of resultant code as well as what parts of the project you implemented (and the ones you couldn’t) resulting from constrained time (3 days) . So before focusing on creating perfect model/conditions, make sure you will have enough time to cover all needed parts of the solution.

**Background**

Model in which you're gonna operate: Banks have customers. Customers have accounts. Accounts hold money. Transfers are done between accounts. Account holds a list of all transfers. 
There can be two types of transfers: 
Intra-bank transfers, between accounts of the same bank. They don't have commissions, they don't have limits and they always succeed. 
Inter-bank transfers, between accounts in different banks. They have R$5 commissions, they have a limit of R$5000 per transfer and they have a 30% chance of failure. 

**Part 1:** Define a set of data structures to accurately reflect the described model. Make sure that new type of transfers can be added with minimal effort.

**Part 2:** Create a Transfer Agent that receives an order to transfer money from account A to account B, it issues transfers to the banks considering commissions, limits and handles the possibility of failures.
Expose the Transfer Agent functionality through a rest controller.
For this test scope consider that the transfer agent holds the banks. 

## Install Docker

To run the project you will need docker, so follow the download and installation instructions through the official documentation provided below:

- linux: https://docs.docker.com/desktop/linux/install/
- mac: https://docs.docker.com/desktop/mac/install/
- windows: https://docs.docker.com/desktop/windows/install/

## Run

To run this project you need Java 11 and postgres 13. 

You can also run with docker, just open a shell in directory docker so run the command below

```sh

cd docker && docker-compose up

```

## Using and Testing

**hoppscotch**

When executing the "Run" step with docker you will be able to access http://localhost:3000. This is a postman-like tool.

Then import the collection named `docker/hoppscotch-collections.json`

------- 

**postman**

If you have postman installed you can import the file `docker/postman-v2.1_collection.json` and run your tests

-------

**curl**

If you find it easier to test with curl, just get the commands from the `docker/curl-collections.md` file

## Run Integration and Unit Tests

Open terminal in root directory so execute:

```sh
docker-compose up -d && ./mvnw test && docker-compose down
```
