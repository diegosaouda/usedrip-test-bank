# curl collections tests

Open a shell terminal so execute a command below:

## Transfer InterBank

```sh
curl -v --location --request POST 'http://localhost:8080/transfer' \
--header 'Content-Type: application/json' \
--data-raw '{
  "fromAccount": "d4fa764d-b551-4dce-8908-2101d63c6630",
  "toAccount": "03f15188-a31b-4a60-83d1-dd99127f96e1",
  "amount": "5.19"
}
'
```

## Transfer IntraBank

```sh
curl -v --location --request POST 'http://localhost:8080/transfer' \
--header 'Content-Type: application/json' \
--data-raw '{
  "fromAccount": "d4fa764d-b551-4dce-8908-2101d63c6630",
  "toAccount": "2e949e4d-914f-4e5e-99b2-57fb790ad30b",
  "amount": "19.50"
}
'
```
