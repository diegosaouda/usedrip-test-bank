package br.com.usedrip.app.service.transfer;

import br.com.usedrip.app.domain.entity.Account;
import br.com.usedrip.app.domain.entity.Bank;
import br.com.usedrip.app.domain.entity.Customer;
import br.com.usedrip.app.exception.CashoutLimitExceededException;
import br.com.usedrip.app.exception.NoFundsException;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

public class InterBankTransferTest {
    private InterBankTransfer transfer = new InterBankTransfer();

    @Test
    public void testSuccess() {
        Account from = fakeAccountBank1();
        Account to = fakeAccountBank2();

        from.setBalance(new BigDecimal(10));

        try {
            transfer.execute(from, to, new BigDecimal(2));
            assertEquals(from.getBalance(), new BigDecimal(8));
            assertEquals(to.getBalance(), new BigDecimal(2));

        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void testNoFundsSuccess() {
        Account from = fakeAccountBank1();
        Account to = fakeAccountBank2();

        from.setBalance(new BigDecimal(10));

        try {
            transfer.execute(from, to, new BigDecimal(12));
            assertEquals(from.getBalance(), new BigDecimal(-2));
            assertEquals(to.getBalance(), new BigDecimal(12));

        } catch (Exception e) {
            fail(e);
        }
    }

    private Account fakeAccountBank1() {
        Customer customer = new Customer("");
        return new Account(customer, new Bank(1, ""));
    }

    private Account fakeAccountBank2() {
        Customer customer = new Customer("");
        return new Account(customer, new Bank(1, ""));
    }

}
