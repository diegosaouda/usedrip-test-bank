package br.com.usedrip.app.service.transfer;

import br.com.usedrip.app.domain.entity.Account;
import br.com.usedrip.app.domain.entity.Bank;
import br.com.usedrip.app.domain.entity.Customer;
import br.com.usedrip.app.exception.CashoutLimitExceededException;
import br.com.usedrip.app.exception.FailTransferIntraBankException;
import br.com.usedrip.app.exception.NoFundsException;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import static org.junit.jupiter.api.Assertions.*;

public class IntraBankTransferTest {
    private IntraBankTransfer transfer = new IntraBankTransfer();

    static {
        IntraBankTransfer.enabledFailSimulation = false;
    }

    @Test
    public void testSuccess() {
        Account from = fakeAccountBank1();
        Account to = fakeAccountBank2();

        from.setBalance(new BigDecimal(10));

        try {
            transfer.execute(from, to, new BigDecimal(2));
            assertEquals(from.getBalance(), new BigDecimal(3));
            assertEquals(to.getBalance(), new BigDecimal(2));

        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void testFailCashoutLimit() {
        Account from = fakeAccountBank1();
        Account to = fakeAccountBank2();

        from.setBalance(new BigDecimal(10_000));

        assertThrowsExactly(
                CashoutLimitExceededException.class,
                () -> transfer.execute(from, to, new BigDecimal(5_000.01)));
    }

    @Test
    public void testFailNoFunds() {
        Account from = fakeAccountBank1();
        Account to = fakeAccountBank2();

        from.setBalance(new BigDecimal(10));

        assertThrowsExactly(
                NoFundsException.class,
                () -> transfer.execute(from, to, new BigDecimal(10)));
    }

    private Account fakeAccountBank1() {
        Customer customer = new Customer("");
        return new Account(customer, new Bank(1, ""));
    }

    private Account fakeAccountBank2() {
        Customer customer = new Customer("");
        return new Account(customer, new Bank(2, ""));
    }

}
