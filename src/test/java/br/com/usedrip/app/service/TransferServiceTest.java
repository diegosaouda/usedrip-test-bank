package br.com.usedrip.app.service;

import br.com.usedrip.app.domain.entity.Account;
import br.com.usedrip.app.domain.repository.AccountRepository;
import br.com.usedrip.app.dto.TransferRequest;

import br.com.usedrip.app.exception.AccountNotFoundException;
import br.com.usedrip.app.exception.FailTransferIntraBankException;
import br.com.usedrip.app.exception.InvalidAmountException;
import br.com.usedrip.app.exception.TransferToSameAccountException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.UUID;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class TransferServiceTest {
    private UUID invalidAccount = UUID.fromString("e4fa764d-b551-4dce-8908-2101d63c6630");
    private UUID fromAccount = UUID.fromString("d4fa764d-b551-4dce-8908-2101d63c6630");
    private UUID toAccount = UUID.fromString("03f15188-a31b-4a60-83d1-dd99127f96e1");

    private UUID toAccountIntraBank = UUID.fromString("2e949e4d-914f-4e5e-99b2-57fb790ad30b");


    @Autowired
    private TransferService transferService;

    @Autowired
    private AccountRepository accountRepository;

    @Test
    public void testTranserInterBankSuccess() {
        BigDecimal amount = new BigDecimal(10);
        TransferRequest transferRequest = new TransferRequest(fromAccount, toAccount, amount);
        assertDoesNotThrow(() -> transferService.execute(transferRequest));

        Account account = accountRepository.findById(fromAccount).get();
        assertEquals(account.getBalance(), new BigDecimal("9989.00"));
    }

    @Test
    public void testAccountNotFound() {
        BigDecimal amount = new BigDecimal(10);
        TransferRequest transferRequest = new TransferRequest(invalidAccount, toAccount, amount);
        assertThrows(AccountNotFoundException.class, () -> transferService.execute(transferRequest));
    }

    @Test
    public void testAmountZero() {
        BigDecimal amount = new BigDecimal(0);
        TransferRequest transferRequest = new TransferRequest(invalidAccount, toAccount, amount);
        assertThrows(InvalidAmountException.class, () -> transferService.execute(transferRequest));
    }

    @Test
    public void testAmountLessZero() {
        BigDecimal amount = new BigDecimal(-1);
        TransferRequest transferRequest = new TransferRequest(fromAccount, toAccount, amount);
        assertThrows(InvalidAmountException.class, () -> transferService.execute(transferRequest));
    }

    @Test
    public void testSameAccountError() {
        BigDecimal amount = new BigDecimal(1);
        TransferRequest transferRequest = new TransferRequest(fromAccount, fromAccount, amount);
        assertThrows(TransferToSameAccountException.class, () -> transferService.execute(transferRequest));
    }

    @Test
    public void testTranserInterBank() {
        BigDecimal amount = new BigDecimal(10);
        TransferRequest transferRequest = new TransferRequest(fromAccount, toAccount, amount);
        try {
            transferService.execute(transferRequest);
        } catch (FailTransferIntraBankException e) {
            // ignore fail simulate
        } catch (Exception e) {
            fail(e);
        }
    }
}
