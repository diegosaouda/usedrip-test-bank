package br.com.usedrip.app.domain.entity;

public enum TransferType {
    IntraBank, InterBank
}
