package br.com.usedrip.app.domain.repository;

import br.com.usedrip.app.domain.entity.Account;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface AccountRepository extends CrudRepository<Account, UUID> {

}
