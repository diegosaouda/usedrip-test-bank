package br.com.usedrip.app.domain.repository;

import br.com.usedrip.app.domain.entity.Customer;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface CustomerRepository extends CrudRepository<Customer, UUID> {

}
