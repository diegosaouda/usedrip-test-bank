package br.com.usedrip.app.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@EqualsAndHashCode
@ToString
@Entity
public class Customer extends BaseEntity {

    @Id
    @Getter
    private UUID id;

    @Getter
    private String name;

    protected Customer() {}

    public Customer(String name) {
        this.name = name;
        this.id = UUID.randomUUID();
    }

    public Customer(String name, UUID id) {
        this.name = name;
        this.id = id;
    }
}
