package br.com.usedrip.app.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.*;

@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@EqualsAndHashCode
@ToString
@Entity
public class Transfer extends BaseEntity {

    @Id
    @Getter
    private UUID id;

    @Getter
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "from_account_id", nullable = false)
    private Account fromAccount;

    @Getter
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "to_account_id", nullable = false)
    private Account toAccount;

    @Getter
    private Date createdAt;

    @Getter
    private BigDecimal amount;

    @Getter
    private BigDecimal fee;

    @Getter
    @Enumerated(EnumType.STRING)
    private TransferType type;

    protected Transfer() {}

    private void InitValues() {
        this.createdAt = new Date();
        this.id = UUID.randomUUID();
        this.fee = new BigDecimal(0);
    }

    public Transfer(Account fromAccount, Account toAccount, BigDecimal amount, TransferType type) {
        InitValues();
        this.fromAccount = fromAccount;
        this.toAccount = toAccount;
        this.amount = amount;
        this.type = type;
    }

    public Transfer(Account fromAccount, Account toAccount, BigDecimal amount, BigDecimal fee, TransferType type) {
        InitValues();
        this.fromAccount = fromAccount;
        this.toAccount = toAccount;
        this.amount = amount;
        this.fee = fee;
        this.type = type;
    }
}
