package br.com.usedrip.app.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;

@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@EqualsAndHashCode
@ToString
@Entity
public class Bank extends BaseEntity {

    @Id
    @Getter
    private Integer code;

    @Getter
    private String name;

    protected Bank() {}
    public Bank(Integer code, String name) {
        this.name = name;
        this.code = code;
    }

    public Bank(Integer code) {
        this.code = code;
    }
}
