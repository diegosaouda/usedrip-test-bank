package br.com.usedrip.app.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.UUID;

@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@EqualsAndHashCode
@ToString
@Entity
public class Account extends BaseEntity {

    @Id
    @Getter
    private UUID id;

    @Getter
    @Setter
    private BigDecimal balance = new BigDecimal(0);

    @Getter
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "customer_id", nullable = false)
    private Customer customer;

    @Getter
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "bank_code", nullable = false)
    private Bank bank;

    protected Account() {}

    public Account(Customer customer, Bank bank) {
        this.id = UUID.randomUUID();
        this.customer = customer;
        this.bank = bank;
    }

    public Account(Customer customer, Bank bank, UUID id) {
        this.id = id;
        this.customer = customer;
        this.bank = bank;
    }
}
