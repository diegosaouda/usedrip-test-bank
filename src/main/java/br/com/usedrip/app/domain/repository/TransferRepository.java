package br.com.usedrip.app.domain.repository;

import br.com.usedrip.app.domain.entity.Transfer;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface TransferRepository extends CrudRepository<Transfer, UUID> {

}
