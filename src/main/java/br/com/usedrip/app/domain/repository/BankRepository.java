package br.com.usedrip.app.domain.repository;

import br.com.usedrip.app.domain.entity.Bank;
import org.springframework.data.repository.CrudRepository;

public interface BankRepository extends CrudRepository<Bank, Integer> {
    Bank findByCode(Integer code);
}
