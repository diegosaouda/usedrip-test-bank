package br.com.usedrip.app.exception;

public class InvalidAmountException extends Exception {
    public InvalidAmountException() {
        super("invalid amount");
    }
}
