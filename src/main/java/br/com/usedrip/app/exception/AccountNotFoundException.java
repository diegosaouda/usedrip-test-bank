package br.com.usedrip.app.exception;

import java.util.UUID;

public class AccountNotFoundException extends Exception {
    public AccountNotFoundException(UUID accountId) {
        super("account '" + accountId + "' not found");
    }
}
