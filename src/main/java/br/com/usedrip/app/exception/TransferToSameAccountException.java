package br.com.usedrip.app.exception;

public class TransferToSameAccountException extends Exception {
    public TransferToSameAccountException() {
        super("transfer to same account error");
    }
}
