package br.com.usedrip.app.exception;

public class FailTransferIntraBankException extends Exception {
    public FailTransferIntraBankException() {
        super("fail transfer intrabank");
    }
}
