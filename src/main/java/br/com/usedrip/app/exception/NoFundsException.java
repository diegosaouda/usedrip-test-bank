package br.com.usedrip.app.exception;

public class NoFundsException extends Exception {
    public NoFundsException() {
        super("no funds");
    }
}
