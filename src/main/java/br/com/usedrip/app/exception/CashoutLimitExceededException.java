package br.com.usedrip.app.exception;

public class CashoutLimitExceededException extends Exception {
    public CashoutLimitExceededException() {
        super("cashout limit exceeded");
    }
}
