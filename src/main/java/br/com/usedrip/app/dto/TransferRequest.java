package br.com.usedrip.app.dto;

import lombok.Data;
import lombok.NonNull;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import java.math.BigDecimal;
import java.util.UUID;

@Data
public class TransferRequest {

    @NonNull
    private UUID fromAccount;

    @NonNull
    private UUID toAccount;

    @NonNull
    @DecimalMin(value = "0.0", inclusive = false)
    @Digits(integer=10, fraction=2)
    private BigDecimal amount;
}
