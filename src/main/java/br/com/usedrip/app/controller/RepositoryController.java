//
// This class is helper to see all datas in database
// In real app don't exists
//

package br.com.usedrip.app.controller;

import br.com.usedrip.app.domain.entity.Account;
import br.com.usedrip.app.domain.entity.Bank;
import br.com.usedrip.app.domain.entity.Customer;
import br.com.usedrip.app.domain.entity.Transfer;
import br.com.usedrip.app.domain.repository.AccountRepository;
import br.com.usedrip.app.domain.repository.BankRepository;
import br.com.usedrip.app.domain.repository.CustomerRepository;
import br.com.usedrip.app.domain.repository.TransferRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/repository")
public class RepositoryController {

    private final BankRepository bankRepository;
    private final CustomerRepository customerRepository;
    private final AccountRepository accountRepository;
    private final TransferRepository transferRepository;

    public RepositoryController(
            BankRepository bankRepository,
            CustomerRepository customerRepository,
            AccountRepository accountRepository,
            TransferRepository transferRepository
    ) {
        this.bankRepository = bankRepository;
        this.customerRepository = customerRepository;
        this.accountRepository = accountRepository;
        this.transferRepository = transferRepository;
    }

    @GetMapping("/banks")
    public Iterable<Bank> allBanks() {
        return bankRepository.findAll();
    }

    @GetMapping("/accounts")
    public Iterable<Account> allAccounts() {
        return accountRepository.findAll();
    }

    @GetMapping("/customers")
    public Iterable<Customer> allCustomers() {
        return customerRepository.findAll();
    }

    @GetMapping("/transfers")
    public Iterable<Transfer> alltransfers() {
        return transferRepository.findAll();
    }
}
