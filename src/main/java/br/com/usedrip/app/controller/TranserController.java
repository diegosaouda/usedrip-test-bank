//
// This class is helper to see all datas in database
// In real app don't exists
//

package br.com.usedrip.app.controller;
import br.com.usedrip.app.dto.TransferRequest;
import br.com.usedrip.app.exception.*;
import br.com.usedrip.app.service.TransferService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/transfer")
public class TranserController {

    private final TransferService transferService;

    public TranserController(TransferService transferService) {
        this.transferService = transferService;
    }

    @PostMapping
    public ResponseEntity transfer(@Valid @RequestBody TransferRequest tranfer) {
        try {
            transferService.execute(tranfer);
            return ResponseEntity.ok().build();
        } catch (AccountNotFoundException | CashoutLimitExceededException | FailTransferIntraBankException |
                 NoFundsException | TransferToSameAccountException | InvalidAmountException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.internalServerError().build();
        }
    }
}
