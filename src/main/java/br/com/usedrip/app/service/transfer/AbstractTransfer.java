package br.com.usedrip.app.service.transfer;

import br.com.usedrip.app.domain.entity.Account;
import br.com.usedrip.app.domain.entity.Transfer;

import java.math.BigDecimal;

public abstract class AbstractTransfer {
    protected final AbstractTransfer next;

    protected AbstractTransfer(AbstractTransfer next) {
        this.next = next;
    }

    public abstract Transfer execute(Account fromAccount, Account toAccount, BigDecimal amount) throws Exception;
}
