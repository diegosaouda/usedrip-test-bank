package br.com.usedrip.app.service.transfer;

import br.com.usedrip.app.domain.entity.Account;
import br.com.usedrip.app.domain.entity.Transfer;

import java.math.BigDecimal;

public class HandlerTransferChain extends AbstractTransfer {

    public HandlerTransferChain() {
        super(new InterBankTransfer());
    }

    @Override
    public Transfer execute(Account fromAccount, Account toAccount, BigDecimal amount) throws Exception {
        return next.execute(fromAccount, toAccount, amount);
    }
}
