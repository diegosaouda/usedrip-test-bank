package br.com.usedrip.app.service;

import br.com.usedrip.app.domain.entity.Account;
import br.com.usedrip.app.domain.entity.Transfer;
import br.com.usedrip.app.domain.repository.AccountRepository;
import br.com.usedrip.app.domain.repository.BankRepository;
import br.com.usedrip.app.domain.repository.CustomerRepository;
import br.com.usedrip.app.domain.repository.TransferRepository;
import br.com.usedrip.app.dto.TransferRequest;
import br.com.usedrip.app.exception.*;
import br.com.usedrip.app.service.transfer.HandlerTransferChain;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Optional;

@Service
public class TransferService {
    private static final HandlerTransferChain handlerTransferChain = new HandlerTransferChain();
    private final AccountRepository accountRepository;
    private final TransferRepository transferRepository;

    public TransferService(
            AccountRepository accountRepository,
            TransferRepository transferRepository
    ) {
        this.accountRepository = accountRepository;
        this.transferRepository = transferRepository;
    }

    @Transactional
    public void execute(TransferRequest transferRequest)
            throws InvalidAmountException, AccountNotFoundException, CashoutLimitExceededException,
                FailTransferIntraBankException, NoFundsException, TransferToSameAccountException, Exception {

        BigDecimal amount = transferRequest.getAmount();

        if (amount.compareTo(new BigDecimal(0)) <= 0) {
            throw new InvalidAmountException();
        }

        Optional<Account> optionalFromAccount = accountRepository.findById(transferRequest.getFromAccount());
        Optional<Account> optionalToAccount = accountRepository.findById(transferRequest.getToAccount());

        if (optionalFromAccount.isEmpty()) {
            throw new AccountNotFoundException(transferRequest.getFromAccount());
        }

        if (optionalToAccount.isEmpty()) {
            throw new AccountNotFoundException(transferRequest.getToAccount());
        }

        Account fromAccount = optionalFromAccount.get();
        Account toAccount = optionalToAccount.get();

        if (fromAccount.getId().equals(toAccount.getId())) {
            throw new TransferToSameAccountException();
        }

        Transfer transfer = handlerTransferChain.execute(fromAccount, toAccount, amount);
        accountRepository.save(fromAccount);
        accountRepository.save(toAccount);
        transferRepository.save(transfer);
    }
}
