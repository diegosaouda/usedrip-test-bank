package br.com.usedrip.app.service.transfer;

import br.com.usedrip.app.domain.entity.Account;
import br.com.usedrip.app.domain.entity.Transfer;
import br.com.usedrip.app.domain.entity.TransferType;
import br.com.usedrip.app.exception.NoFundsException;

import java.math.BigDecimal;

public class InterBankTransfer extends AbstractTransfer {

    protected InterBankTransfer() {
        super(new IntraBankTransfer());
    }

    @Override
    public Transfer execute(Account fromAccount, Account toAccount, BigDecimal amount) throws Exception {

        if (fromAccount.getBank().getCode() != toAccount.getBank().getCode()) {
            return next.execute(fromAccount, toAccount, amount);
        }

        fromAccount.setBalance(fromAccount.getBalance().subtract(amount));

        // for interbank accounts we let the customer go negative
        // simulating different rules between different chains

//        if (fromAccount.getAmount().compareTo(BigDecimal.ZERO) == -1) {
//            throw new NoFundsException();
//        }

        toAccount.setBalance(toAccount.getBalance().add(amount));
        return new Transfer(fromAccount, toAccount, amount, TransferType.IntraBank);
    }
}
