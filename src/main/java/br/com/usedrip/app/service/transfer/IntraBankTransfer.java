package br.com.usedrip.app.service.transfer;

import br.com.usedrip.app.domain.entity.Account;
import br.com.usedrip.app.domain.entity.Transfer;
import br.com.usedrip.app.domain.entity.TransferType;
import br.com.usedrip.app.exception.CashoutLimitExceededException;
import br.com.usedrip.app.exception.ChainException;
import br.com.usedrip.app.exception.FailTransferIntraBankException;
import br.com.usedrip.app.exception.NoFundsException;

import java.math.BigDecimal;
import java.util.Random;

public class IntraBankTransfer extends AbstractTransfer {

    public static boolean enabledFailSimulation = true;

    private final BigDecimal fee = new BigDecimal(5);
    private final BigDecimal limitCashout = new BigDecimal(5_000);

    protected IntraBankTransfer() {
        super(null);
    }

    public Transfer execute(Account fromAccount, Account toAccount, BigDecimal amount)
            throws CashoutLimitExceededException, FailTransferIntraBankException, NoFundsException {

        // shouldn't get here with this kind of validation
        if (fromAccount.getBank().getCode() == toAccount.getBank().getCode()) {
            throw new ChainException();
        }

        throwIfFailPercent30();
        throwIfCashoutLimitExceeded(amount);

        fromAccount.setBalance(fromAccount.getBalance().subtract(amount.add(fee)));
        if (fromAccount.getBalance().compareTo(BigDecimal.ZERO) == -1) {
            throw new NoFundsException();
        }

        toAccount.setBalance(toAccount.getBalance().add(amount));
        return new Transfer(fromAccount, toAccount, amount, fee, TransferType.IntraBank);
    }

    private void throwIfCashoutLimitExceeded(BigDecimal amount) throws CashoutLimitExceededException {
        if (amount.compareTo(limitCashout) == 1) {
            throw new CashoutLimitExceededException();
        }
    }

    private void throwIfFailPercent30() throws FailTransferIntraBankException {
        if (enabledFailSimulation && new Random().nextInt(10) < 3) {
            throw new FailTransferIntraBankException();
        }
    }
}
