package br.com.usedrip.app.service;

import br.com.usedrip.app.domain.entity.Account;
import br.com.usedrip.app.domain.entity.Bank;
import br.com.usedrip.app.domain.entity.Customer;
import br.com.usedrip.app.domain.repository.AccountRepository;
import br.com.usedrip.app.domain.repository.BankRepository;
import br.com.usedrip.app.domain.repository.CustomerRepository;
import br.com.usedrip.app.domain.repository.TransferRepository;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.UUID;

@Service
public class InitDatabaseService {
    private final AccountRepository accountRepository;
    private final TransferRepository transferRepository;
    private final BankRepository bankRepository;
    private final CustomerRepository customerRepository;

    public InitDatabaseService(
            BankRepository bankRepository,
            CustomerRepository customerRepository,
            AccountRepository accountRepository,
            TransferRepository transferRepository
    ) {
        this.accountRepository = accountRepository;
        this.customerRepository = customerRepository;
        this.transferRepository = transferRepository;
        this.bankRepository = bankRepository;
    }

    public void execute() {
        bankRepository.save(new Bank(1, "Bank Purple"));
        bankRepository.save(new Bank(2, "Bank Yellow"));
        bankRepository.save(new Bank(3, "Bank Red"));

        Customer customer1 = new Customer("Sydney Martin", UUID.fromString("2a647d8f-2b98-4193-afed-dc05c4606cca"));
        Customer customer2 = new Customer("Kelly Miller", UUID.fromString("67c86c70-d378-466b-9a6a-2f44ab749ebb"));
        Customer customer3 = new Customer("Kimberly Martin", UUID.fromString("dc6cb69b-e3d6-4543-90ba-4fbf53d0cea6"));

        customerRepository.save(customer1);
        customerRepository.save(customer2);
        customerRepository.save(customer3);

        Account account1 = new Account(customer1, new Bank(1), UUID.fromString("d4fa764d-b551-4dce-8908-2101d63c6630"));
        account1.setBalance(new BigDecimal(9_999));

        Account account2 = new Account(customer2, new Bank(2), UUID.fromString("2e949e4d-914f-4e5e-99b2-57fb790ad30b"));
        account2.setBalance(new BigDecimal(9_999));

        Account account3 = new Account(customer3, new Bank(1), UUID.fromString("03f15188-a31b-4a60-83d1-dd99127f96e1"));
        account3.setBalance(new BigDecimal(9_999));

        // verify to not replace amount balance
        if (accountRepository.findById(account1.getId()).isEmpty()) {
            accountRepository.save(account1);
        }

        if (accountRepository.findById(account2.getId()).isEmpty()) {
            accountRepository.save(account2);
        }

        if (accountRepository.findById(account3.getId()).isEmpty()) {
            accountRepository.save(account3);
        }
    }
}
